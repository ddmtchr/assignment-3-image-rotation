#include "../include/validation.h"

#define DECIMAL 10
#define MAXIMUM_NUMBER_MODULUS 270
#define ROTATION_STEP 90

int32_t parse_angle(char* str) {
    char* end = "";
    int32_t num = (int) strtol(str, &end, DECIMAL);
    if (*end != '\0') return 1;
    return num;
}

static bool angle_is_valid(char* str_angle) {
    int64_t angle = parse_angle(str_angle);
    return (angle >= -MAXIMUM_NUMBER_MODULUS && angle <= MAXIMUM_NUMBER_MODULUS
        && angle % ROTATION_STEP == 0);
}

enum args_validate_status validate_args(int argc, char** argv) {
    if (argc != 4) {
        return INVALID_ARGS_NUMBER;
    }
    if (!angle_is_valid(argv[3])) {
        return INVALID_ANGLE;
    }
    return ARGS_OK;
}
