const char* validation_error_messages[] = {
        "Args are ok",
        "Usage: ./image-transformer <source-image> <transformed-image> <angle>",
        "Angle must be one of: -270 -180 -90 0 90 180 270"
};

const char* reading_error_messages[] = {
        "Read successfully",
        "Header reading error",
        "Image reading error",
        "Out of memory"
};

const char* writing_error_messages[] = {
        "Wrote successfully",
        "Header writing error",
        "Image writing error",
        "Out of memory"
};
