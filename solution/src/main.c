#include <stdio.h>

#include "../include/bmp.h"
#include "../include/files.h"
#include "../include/io.h"
#include "../include/rotation.h"
#include "../include/validation.h"

#define FULL_TURN 360

int main(int argc, char** argv) {
    uint8_t errors = 0;
    int8_t status = validate_args(argc, argv);
    if (status != ARGS_OK) {
        print_validation_error(status);
        return 1;
    }

    FILE* file_in = open_file_byte_reading(argv[1]);
    if (!file_in) {
        print_error("File opening error, check path and permissions");
        return 1;
    }

    struct image img_in = {0};

    status = from_bmp(file_in, &img_in);
    if (status != READ_OK) {
        print_reading_error(status);
        return 1;
    }
    if (close_file(file_in) != 0) {
        print_error("File closing error");
        errors++;
    }
    puts("Processing...");

    int32_t angle = parse_angle(argv[3]);
    angle = angle < 0 ? angle + FULL_TURN : angle;
    struct image img_out = rotate(img_in, angle);
    free(img_in.data);
    if (!img_out.data) {
        print_writing_error(WRITE_OUT_OF_MEMORY);
        return 1;
    }

    FILE* file_out = create_file_byte_writing(argv[2]);
    if (!file_out) {
        free(img_out.data);
        print_error("Error creating output file");
        return 1;
    }

    status = to_bmp(file_out, &img_out);
    free(img_out.data);
    if (status != WRITE_OK) {
        print_writing_error(status);
        errors++;
    }
    if (close_file(file_out) != 0) {
        print_error("File closing error");
        errors++;
    }

    if (errors) puts("Image rotated with some errors");
    else puts("Image rotated successfully");
    return 0;
}
