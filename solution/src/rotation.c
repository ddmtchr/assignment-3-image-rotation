#include "../include/rotation.h"

#include <stdlib.h>

#include "../include/util.h"

static size_t get_old_pos(size_t i, size_t j, uint64_t h, uint64_t w, int32_t angle, size_t s_padding) {
    switch (angle) {
        case 90:
            return (3 * w + s_padding) * j + 3 * (w - 1 - i);
        case 180:
            return (3 * w + s_padding) * (h - 1 - i) + 3 * (w - 1 - j);
        case 270:
            return (3 * w + s_padding) * (h - 1 - j) + 3 * i;
        default:
            return (3 * w + s_padding) * i + 3 * j;
    }
}

struct image rotate(struct image source, int32_t angle) {
    struct image target = {0};
    if (angle % 180 == 0) {
        target.height = source.height;
        target.width = source.width;
    } else {
        target.height = source.width;
        target.width = source.height;
    }

    size_t source_padding = get_padding(source.width);
    size_t target_padding = get_padding(target.width);
    size_t target_width_in_bytes = target.width * sizeof(struct pixel) + target_padding;

    struct pixel *old_data = source.data;
    struct pixel *new_data = malloc(target_width_in_bytes * target.height);
    if (!new_data) return target;

    uint8_t *bytes_old_data = (uint8_t *) old_data;
    uint8_t *bytes_new_data = (uint8_t *) new_data;

    size_t pos = 0;
    for (size_t i = 0; i < target.height; i++) {
        for (size_t j = 0; j < target.width; j++, pos += sizeof(struct pixel)) {
            size_t old_pos = get_old_pos(i, j, source.height, source.width, angle, source_padding);

            bytes_new_data[pos] = bytes_old_data[old_pos];
            bytes_new_data[pos + 1] = bytes_old_data[old_pos + 1];
            bytes_new_data[pos + 2] = bytes_old_data[old_pos + 2];
        }
        for (size_t j = 0; j < target_padding; j++, pos++) {
            bytes_new_data[pos] = 0;
        }
    }
    target.data = new_data;
    return target;
}
