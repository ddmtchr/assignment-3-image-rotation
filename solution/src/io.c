#include "../include/io.h"

#include <stdio.h>

#include "../include/errormessages.h"

void print_error(const char* message) {
    fprintf(stderr, "%s\n", message);
}

void print_validation_error(enum args_validate_status status) {
    print_error(validation_error_messages[status]);
}

void print_reading_error(enum read_status status) {
    print_error(reading_error_messages[status]);
}

void print_writing_error(enum write_status status) {
    print_error(writing_error_messages[status]);
}
