#include "../include/util.h"

size_t get_padding(uint64_t width) {
    return 4 - (width * 3 % 4);
}
