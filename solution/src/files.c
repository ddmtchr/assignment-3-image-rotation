#include "../include/files.h"

FILE* open_file_byte_reading(const char* filename) {
    FILE* file = fopen(filename, "rb");
    return file;
}

FILE* create_file_byte_writing(const char* filename) {
    FILE* file = fopen(filename, "wb");
    return file;
}

int close_file(FILE* file) {
    return fclose(file);
}
