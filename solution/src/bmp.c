#include "../include/bmp.h"

#include <stdlib.h>

#include "../include/util.h"

#define BMP_FTYPE 19778
#define BMP_OFFBITS 54
#define BMP_ISIZE 40
#define BMP_PLANES 1
#define BMP_BITCOUNT 24
#define BMP_PELS_PER_METER 2834

static void init_bmp_image(struct image* img, const struct bmp_header* header) {
    img->height = header->biHeight;
    img->width = header->biWidth;
    img->data = malloc(header->biSizeImage);
}

static void fill_bmp_header(struct bmp_header* header, const struct image* img) {
    uint64_t width_in = img->width;
    uint64_t height_in = img->height;
    size_t padding = get_padding(width_in);

    header->bfType = BMP_FTYPE;
    header->bfileSize = (width_in * sizeof(struct pixel) + padding) * height_in + BMP_OFFBITS;
    header->bfReserved = 0;
    header->bOffBits = BMP_OFFBITS;
    header->biSize = BMP_ISIZE;
    header->biWidth = width_in;
    header->biHeight = height_in;
    header->biPlanes = BMP_PLANES;
    header->biBitCount = BMP_BITCOUNT;
    header->biCompression = 0;
    header->biSizeImage = (width_in * sizeof(struct pixel) + padding) * height_in;
    header->biXPelsPerMeter = BMP_PELS_PER_METER;
    header->biYPelsPerMeter = BMP_PELS_PER_METER;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

static size_t read_header(FILE* in, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, in);
}

static size_t write_header(FILE* out, struct bmp_header* header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out);
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header_in = {0};

    if (read_header(in, &header_in) != 1) {
        return READ_INVALID_HEADER;
    }
    init_bmp_image(img, &header_in);

    if (!img->data) {
        return READ_OUT_OF_MEMORY;
    }

    if (fread(img->data, header_in.biSizeImage, 1, in) != 1) {
        free(img->data);
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header_out = {0};
    fill_bmp_header(&header_out, img);

    if (write_header(out, &header_out) != 1) {
        return WRITE_HEADER_ERROR;
    }

    if (fwrite(img->data, header_out.biSizeImage, 1, out) != 1) {
        return WRITE_IMAGE_ERROR;
    }
    return WRITE_OK;
}
