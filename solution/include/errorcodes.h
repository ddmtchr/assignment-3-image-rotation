#ifndef IMAGE_TRANSFORMER_ERRORCODES_H
#define IMAGE_TRANSFORMER_ERRORCODES_H

enum args_validate_status {
    ARGS_OK = 0,
    INVALID_ARGS_NUMBER = 1,
    INVALID_ANGLE = 2
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER = 1,
    READ_INVALID_BITS = 2,
    READ_OUT_OF_MEMORY = 3
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR = 1,
    WRITE_IMAGE_ERROR = 2,
    WRITE_OUT_OF_MEMORY = 3
};

#endif
