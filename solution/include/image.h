#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <inttypes.h>

#ifdef _MSC_VER
#define __attribute__(x)
#endif

#pragma pack(push, 1)
struct __attribute__((packed)) pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
