#ifndef IMAGE_TRANSFORMER_FILES_H
#define IMAGE_TRANSFORMER_FILES_H

#include <stdio.h>

FILE* open_file_byte_reading(const char* filename);

FILE* create_file_byte_writing(const char* filename);

int close_file(FILE* file);

#endif
