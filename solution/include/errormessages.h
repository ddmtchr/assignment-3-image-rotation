#ifndef IMAGE_TRANSFORMER_ERRORMESSAGES_H
#define IMAGE_TRANSFORMER_ERRORMESSAGES_H

extern const char* validation_error_messages[];

extern const char* reading_error_messages[];

extern const char* writing_error_messages[];

#endif
