#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <inttypes.h>
#include <stddef.h>

size_t get_padding(uint64_t width);

#endif
