#ifndef IMAGE_TRANSFORMER_VALIDATION_H
#define IMAGE_TRANSFORMER_VALIDATION_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../include/errorcodes.h"

int32_t parse_angle(char* str);

enum args_validate_status validate_args(int argc, char** argv);

#endif
