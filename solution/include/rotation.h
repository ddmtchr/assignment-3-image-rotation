#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

#include <inttypes.h>

#include "../include/image.h"

struct image rotate(struct image source, int32_t angle);

#endif
