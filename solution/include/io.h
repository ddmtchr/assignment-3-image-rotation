#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

#include "../include/errorcodes.h"

void print_error(const char* message);

void print_validation_error(enum args_validate_status status);

void print_reading_error(enum read_status status);

void print_writing_error(enum write_status status);

#endif
